window.addEventListener("load", function () {
	/**
	*@main setting up all the modules so they can talk to eachother
	*/

	var da = acme.RecipeDataAccess();

	/**
	* @property recipeDetails sets up function to post or put data 
	* @method putRecipe gets the data and passes it into function to be put back into db
	* @method postRecipe gets data and passes it into function to be posted new to db
	*/

	var recipeDetails = acme.RecipeDetails("recipe-details", function (data) {

			if(data.id > 0){
				da.putRecipe(data, function(response){
					console.log(response);
				});
			
			}else if(data.id == 0 || data.id == ""){
				da.postRecipe(data, function(response){
					console.log(response);
				});
			}

			recipeDetails.validate();
			refresh();
	});

	/**
	* @method setEditCallBack  puts data into recipeDetails input boxes when edit button is clicked
	* @param recipe  data gets passed in from recipeList and gets set to input boxes in recipeDetails
	*/


	var recipeList = acme.RecipeList("recipe-list");
		recipeList.setEditCallback(function(recipe){
			
			recipeDetails.setRecipe(recipe);

		});

	/**
	* @method getAllRecipes  gets data from response from RecipeDataAccess class and parses it and sets up recipeList
	* @param response  passes response into variable recipes after parsing it
	*/


	var da = acme.RecipeDataAccess();
	da.getAllRecipes(function(response){

		
		var recipes = JSON.parse(response);
		recipeList.setRecipes(recipes);


	});

	/**
	* @method refresh  sends ajax request to get the data to refresh the page and display new data
	* 
	*/

	function refresh(){
		document.getElementById("recipe-list").innerHTML= "";

		acme.ajax.send({
			 callback: recipeDetails.setRecipe,
			 url: "https://localhost/adv-topics-final/web-service/",
			 method: "GET"
			 });
		
		
		da.getAllRecipes(function(response){

			var recipes = JSON.parse(response);
			recipeList.setRecipes(recipes);
			recipeDetails.clearFields();
			
		});
			
		
	 }


});