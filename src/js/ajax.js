/**
* @module acme
*/

var acme = acme || {};

acme.ajax = {

    /**
    * Sends an XMLHttpRequest and returns the response in a callback.
    *
    * @method send
    * @param {Function} options.callback     This function will be triggers upon success it will pass in the response as a param
    * @param {string} options.url
    * @param {Object} options.method
    * @param {Object} [options.headers=null]
    * @param {Object} options.requestBody
    * @param {Object} [options.errorCallback=null]
    */

	send: function(options){

		var http = new XMLHttpRequest();
		var callback = options.callback;
		var method = options.method;
		var url = options.url;
		var headers = options.headers;
		var requestBody = options.requestBody;
		var errorCallback = options.errorCallback;

		var http = new XMLHttpRequest();
			http.open(method, url);

		for(var p in headers){
			http.setRequestHeader(p, headers[p]);
		}

		// send the XmlHttpRequest, and if the http.readyState == 4 and the http.status == 200
		http.addEventListener('readystatechange', function() {
			if(http.readyState == 4 && http.status == 200) {
		    	callback(http.responseText);
		    }else if(http.readyState == 4){
		    	if(errorCallback){
		    		errorCallback(http.status);
		    	}else{
		    		alert(http.status);
		    	}
		    }    

		});

		http.send(requestBody);
		

	},
	error: function(errMsg){

		alert(errMsg);
	}
};