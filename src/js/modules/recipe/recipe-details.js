/**
* @module acme
*/

var acme = acme || {};

acme.RecipeDetails = function(id, callback) {

    var elem = document.getElementById(id);
    var da = acme.RecipeDataAccess();

    if (elem == null) {
        throw new Error("The id passed in does not match an element");
    }

    /**
    *
    * @property setting up the template and getting a handle on each input
    * @type DOM elements
    */

    elem.innerHTML = getTemplate();

    var txtID = elem.querySelector(".id");
    var txtTitle = elem.querySelector(".recipeTitle");
    var txtRecipeInstructions = elem.querySelector(".recipeInstructions");
    var txtRecipeIngredients = elem.querySelector(".recipeIngredients");
    var txtRecipeLength = elem.querySelector(".recipeLength");
    var chkIsActive = elem.querySelector(".isActive");
    var btnSaveRecipe = elem.querySelector(".btnSaveRecipe");
    var btnDeleteRecipe = elem.querySelector(".btnDeleteRecipe");
    
    /**
    * @method onclick when save button clicked, validate data and callback
    * @return data in callback and then clear fields
    */

    btnSaveRecipe.onclick= function () {

            if (validate() && callback) {
                var data = {
                    id: txtID.value,
                    title: txtTitle.value,
                    recipe_instructions: txtRecipeInstructions.value,
                    recipe_ingredients: txtRecipeIngredients.value,
                    recipe_length: txtRecipeLength.value,
                    is_active: chkIsActive.value
                    
                };
             
            }

        callback(data);
        clearFields();

    };

    /**
    * @method onclick clears fields when clicked
    */

    btnDeleteRecipe.onclick = function (){
        clearFields();
    };

    /**
    * @method getTemplate  
    * @property html gets set to a template for the input boxes and validation
    * @return {Object} html  returns the html object to display form on page
    */

    function getTemplate() {

        var html = '<ul class = "form-style-1"><br/> \
                        <span class="validation recipe-id"></span><br/> \
                        <input type="hidden" class="id"/> <br> \
                        <b>Recipe Title:</b><br /> \
                        <span class="validation recipe-title"></span><br/> \
                        <input type="text" class="recipeTitle"/> <br> \
                        <br /> \
                        <b>Recipe Instructions:</b><br> \
                        <span class="validation recipe-instructions"></span><br /> \
                        <textarea type="text" class="recipeInstructions field-textarea"/></textarea><br> \
                        <br /> \
                        <b>Recipe Ingredients:</b><br> \
                        <span class="validation recipe-ingredients"></span><br /> \
                        <textarea type="text" class="recipeIngredients field-textarea"></textarea><br> \
                        <br /> \
                        <b>Recipe Length:</b><br> \
                        <span class="validation recipe-length"></span><br /> \
                        <input type="text" class="recipeLength"/><br> \
                        <br /> \
                        <b>is Active:</b><br> \
                        <span class="validation is-active"></span><br /> \
                        <input type="checkbox" class="isActive"/><br> \
                        <br /> \
                        <button type="submit" class="btnSaveRecipe">Save Recipe</button><br></br> \
                        <button type="submit" class="btnDeleteRecipe">Clear Recipe</button> \
                        </ul>';

        return html;
    }

    /**
    * @method validate  validates data getting passed into the form and if there is no data
    * @return isValid   returns the validation messages
    */

    function validate() {

        var isValid = true;
        vRecipeId = elem.querySelector(".recipe-id");
        vRecipeTitle = elem.querySelector(".recipe-title");
        vRecipeInstructions = elem.querySelector(".recipe-instructions");
        vRecipeIngredients = elem.querySelector(".recipe-ingredients");
        vRecipeLength = elem.querySelector(".recipe-length");
        vIsActive = elem.querySelector(".is-active");
        var focusOn = null;


        // clear out the validation messages
        var validationDivs = elem.querySelectorAll(".validation");

        for (var k in validationDivs) {
            validationDivs[k].innerHTML = "";
        }

        if (txtTitle.value == "") {
            isValid = false;
            vRecipeTitle.innerHTML = "Please enter your recipe title.";
            if (focusOn == null) {
                focusOn = txtTitle;
            }
        }

        if (txtRecipeInstructions.value == "") {
            isValid = false;
            vRecipeInstructions.innerHTML = "Please enter some instructions for your recipe!";
            if (focusOn == null) {
                focusOn = txtRecipeInstructions;
            }
        }

        if (txtRecipeIngredients.value == "") {
            isValid = false;
            vRecipeIngredients.innerHTML = "Please enter some ingredients for your recipe!";
            if (focusOn == null) {
                focusOn = txtRecipeIngredients;
            }
        }

        if (txtRecipeLength.value == "") {
            isValid = false;
            vRecipeLength.innerHTML = "Please enter your recipe length";
            if (focusOn == null) {
                focusOn = txtRecipeLength;
            }
        }

        if (isValid == false) {
            focusOn.focus();
        }

        return isValid;
    }

    /**
    * @method clearFields  clears all text boxes
    *
    */


    function clearFields() {
        txtID.value = " ";
        txtTitle.value = " ";
        txtRecipeInstructions.value = " ";
        txtRecipeIngredients.value = " ";
        txtRecipeLength.value = " ";
    }


    /**
    * @method setRecipe  sets recipes values from db into the textboxes
    *
    */

    function setRecipe(newRecipe) {
        recipe = newRecipe;
        txtID.value = recipe.id;
        txtTitle.value = recipe.title;
        txtRecipeInstructions.value = recipe.recipe_instructions;
        txtRecipeIngredients.value = recipe.recipe_ingredients;
        txtRecipeLength.value = recipe.recipe_length;

    }


    /**
    *
    * @return {Object} public API to main.js to be used
    */

    // Return the public API
    return {
        setRecipe: setRecipe,
        clearFields: clearFields,
        validate: validate

    };


};