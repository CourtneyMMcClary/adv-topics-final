/**
* @module acme
*/

var acme = acme || {};

acme.RecipeList = function(id) {
    
    // Instance Vars
   
    var elem = document.getElementById(id);
    var editCallback = null;
    var recipes = [];

   
    if (elem == null) {
        throw new Error("The id passed in does not match an element");
    }

    elem.appendChild(createRecipeList());
    

    // Methods

    /**
    * @method setEditCallback
    * @param 
    */

    function setEditCallback(fn){
        editCallback = fn;
    }


    /**
    * @method setRecipes  sets the empty array of recipes equal to data being passed in and appends createRecipeList to elem DOM object
    * @param{Object} data  A data(recipe) object to be put into an array
    */

    function setRecipes(data){
        recipes = data;
        elem.appendChild(createRecipeList());
    }
 

    /**
    * @method createRecipeList   creates unordered list and loops through recipes to display them on screen 
    * @return unlist  returns data to list from db
    */

    function createRecipeList(){
        var unlist = document.createElement("ul");

        for (var i = 0; i < recipes.length; i++) {

            var div = document.createElement("div");
            div.innerHTML = getTemplate();

            var li = div.querySelector("li");

            var title = li.querySelector(".title");
            title.innerHTML = recipes[i].title;
            var instructions = li.querySelector(".recipe_instructions");
            instructions.innerHTML = recipes[i].recipe_instructions;
            var ingredients = li.querySelector(".recipe_ingredients");
            ingredients.innerHTML = recipes[i].recipe_ingredients;
            var recipeLength = li.querySelector(".recipe_length");
            recipeLength.innerHTML = recipes[i].recipe_length;
            var btnEdit = li.querySelector(".edit");
            
            btnEdit.recipe = recipes[i];
            
            btnEdit.addEventListener('click', function(evt){
                console.log(evt.target.recipe);               

                if(editCallback){
                    editCallback(evt.target.recipe);
                }

            });

           unlist.appendChild(li);

        }


        return unlist;
    }


    /**
    * @method getTemplate  creates html form to be displayed on page
    * @return {Object} html  returns html object to be displayed on the form 
    */

    function getTemplate(){
        var html = '<li class = "form-style-1"><h2 class="title"></h2> \
                    <h3 style="text-decoration: underline">Instructions:</h3> \
                    <h4 class="recipe_instructions"></h4> \
                    <h3 style="text-decoration: underline">Ingredients:</h3> \
                    <h4 class="recipe_ingredients"></h4> \
                    <h3 style="text-decoration: underline">Recipe Length:</h3> \
                    <h4 class="recipe_length"></h4> \
                    <input type="button" class="edit" value="EDIT" /></li>';

        return html;
    }


    /**
    * @method getRecipeById  
    *
    */
    
    function getRecipeById(id) {
        for (var x = 0; x < recipes.length; x++) {
            if (id == recipes[x].id) {
                return recipes[x];
            }
        }
    }


  
    /**
    * @return {Object} public API to main.js to be used
    */

    // Return the public API
    return {
        setRecipes: setRecipes,
        setEditCallback: setEditCallback
    };



};