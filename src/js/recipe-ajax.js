/**
* @module acme
*/
var acme = acme || {};

//this module depends on the acme.ajax module 

acme.RecipeDataAccess = function(){


		/**
		* @method handleResponse  generic callback for module 
		* @param {Object} response    passing in response object and alerting it 
		*/
		function handleResponse(response){
			alert(response);
		}
		
		/**
		* @property recipeURL  checks to see if on dev environment or live service and then passes into each function
		*/

		var recipeURL;
		if(location.host == "localhost"){
			recipeURL = "https://localhost/adv-topics-final/web-service/";
		}else{
			recipeURL = "https://www.cmcclary.com/adv-topics-final/web-service/";
		}
		//VARIABLES
		//this module depends on the acme ajax module
		var ajax = acme.ajax;
		//Note that to make this module more testable, we should pass acme.ajax

		if(!ajax){
			throw new Error("This module depends on the acme.ajax module");
		}

		/**
		* @method getAllRecipes  gets all the recipes from the db 
		* @param {object} callback  passed into the send callback 
		*/

		function getAllRecipes(callback){
			acme.ajax.send({
				callback:callback,
				url: recipeURL+ "recipes",
				method:"GET"
			});
		}

		/**
		* @method getRecipeById  gets recipe by id
		* @param {object} id  passes id into the URL to pull from web service
		* @param {object} callback   passes into the callback in the send
		*/

		function getRecipeById(id, callback){

			acme.ajax.send({
				callback:callback,
				url: recipeURL + "/" + id,			
				method:"GET"
			});
			
		}

		/**
		* @method postRecipe  posts new recipe to db
		* @param {Object} recipe  puts recipe object into requestBody and stringifys it
		* @param {Object} callback  passes into the callback in the send
		*/

		function postRecipe(recipe, callback){

			acme.ajax.send({
				callback:callback,
				url:recipeURL + "/recipes",
				method:"POST",
				requestBody:JSON.stringify(recipe)
				
			});

		}
	
		/**
		* @method putRecipe  gets recipe by id from db and puts it back to the same id when done
		* @param {Object} recipe  puts recipe object into requestBody and stringifys it
		* @param {Object} callback  passes into the callback in the send
		*/
		

		function putRecipe(recipe, callback){
			
			acme.ajax.send({
				callback:callback,
				url: recipeURL + "recipes/" + recipe.id,
				method:"POST",
				requestBody:JSON.stringify(recipe)
			
			});
		
		}

	/**
    * @return {Object} each method gets returned to public API
    */

		return{
			getAllRecipes: getAllRecipes,
			getRecipeById: getRecipeById,
			postRecipe: postRecipe,
			putRecipe: putRecipe
		};

};