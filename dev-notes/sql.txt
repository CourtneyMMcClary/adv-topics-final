--It's not a bad idea to save your sql queries (including create statements) and sample data

//SQL for creating recipe table and columns

CREATE TABLE `recipes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `recipe_instructions` varchar(255) DEFAULT NULL,
  `recipe_ingredients` varchar(255) DEFAULT NULL,
  `recipe_length` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

//SQL for inserting dummy data into db
INSERT INTO `recipes` (`id`, `title`, `recipe_ingredients`, `recipe_instructions`, `recipe_length`) 
VALUES (1, 'Mashed Potatoes', 'Potatoes & Butter', 'Boil Potatoes and then mash and add butter', '30 min'), 
(2, 'Puppy Chow', 'Chex Mix, Powered Sugar, Peanut Butter & Chocolate', 'put chex mix in a bowl and coat in melted chocolate and peanut butter and coat with powered sugar', '25 min'), 
(3, 'Scrambled Eggs', 'Eggs and Milk. Salt and Pepper: optional', 'crack eggs into a bowl and add milk. Mix with fork and pour into pan', '7 min')