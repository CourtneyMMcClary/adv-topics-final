<?php

header("Access-Control-Allow-Origin: *");
///////////////////////////////////
// Set up the data access object
///////////////////////////////////
$link = mysqli_connect("localhost", "cmcclary_cmm", "M0htT72t1l", "cmcclary_recipe_web_service");
require("RecipeDataAccess.inc.php");
$da = new RecipeDataAccess($link);


///////////////////////////////////
// Handle the Request
///////////////////////////////////

// Gather all the information about the request
$url_path = isset($_GET['url_path']) ? $_GET['url_path'] : ""; // This is the path part of the URL being requested (see the .htaccess file)
$method = $_SERVER['REQUEST_METHOD'];
$request_body = file_get_contents('php://input'); // note that GET requests do not have request body
$request_headers = getallheaders();


// This IF statement will check to see if the requested URL (and method) is supported by this web service
// If so, then we need to formulate the proper response, if not then we return a 404 status code
if($method == "GET" && empty($url_path)){

 	//Show the home page for this web service
  //require("api-docs.php");
  die();

}else if($method == "GET" && ($url_path == "recipes/" || $url_path == "recipes")){

  get_all_recipes();

}else if($method == "POST" && ($url_path == "recipes/" || $url_path == "recipes")){
  	
  insert_recipe();
  
}else if($method == "GET" && preg_match('/^recipes\/([0-9]*\/?)$/', $url_path, $matches)){
   
  // Get the id of the Recipe from the regular expression
  $recipe_id = $matches[1];
  get_recipe_by_id($recipe_id);

}else if($method == "POST" && preg_match('/^recipes\/([0-9]*\/?)$/', $url_path, $matches)){

  // Get the id of the recipe from the regular expression
  //$recipe_id = $matches[1]; // we can get the recipe id from the request body
  update_recipe();  
  
}else{

  header('HTTP/1.1 404 Not Found', true, 404);
  die("We're sorry, we can't find this page: {$_SERVER['REQUEST_URI']}");

}


/////////////////////////////////
// FUNCTIONS
/////////////////////////////////

function get_all_recipes(){

  global $da, $url_path, $method, $request_body, $request_headers;
  
  // check to see if we need to sort the recipes by author or title
  $order_by = isset($_GET['order_by']) ? $_GET['order_by'] : null;
  
  // get the recipes from the database
  $recipes = $da->get_all_recipes($order_by);
  if(!$recipes){
    header('HTTP/1.1 500 server error (fetching recipes from the database)', true, 500);
    die();
  }
    
  // We'll default to returning recipes in JSON format unless the client
  // requests XML (the Accept header in the request would be set to application/xml)
  $return_format = $request_headers['Accept'];
  
  if($return_format == "application/xml"){
    //header("Content-Type","application/xml");
    die("TODO: convert recipe data to XML");
  }else{
    //header("Content-Type","application/json");
    $json = json_encode($recipes);
    echo($json);
    die();
  }

}


function insert_recipe(){

  global $da, $url_path, $method, $request_body, $request_headers;

  // The data for the Recipe being inserted should be in the request body.
  // The data should be sent in the JSON format which means that the Content-Type header in the request SHOULD be set to application/json
  // But if it's not set properly, we'll just assume the data is coming in as JSON
  // In the future, we might also accept the data in the request body to be XML, which would mean that the Content-Type header is set to application/xml
  
  $new_recipe = null;
  
  if($request_headers['Content-Type'] == "application/xml"){
    die("TODO: convert XML recipe data into an associative array");    
  }else{
    // convert the json to an associative array
    // note: json_decode() will return false if it can't convert the request body (maybe because it's not valid json)
    $new_recipe = json_decode($request_body, true);
  }

  // TODO: validate the $new_Recipe assoc array (make sure it has id, title and author keys)

  if($new_recipe){
    
    if($new_recipe = $da->insert_recipe($new_recipe)){
      // note that the recipe returned by insert_recipe() will have the id set (by the database auto increment)
      // TODO: if the Accept header in the request is set to application/xml, then the client want the data returned in XML, we could deal with that later  
      // For now, we'll just return the data in JSON format
     // header("Content-Type","application/json");
      die(json_encode($new_recipe));
    }else{
      header('HTTP/1.1 500 server error (fetching recipes from the database)', true, 500);
      die();  
    }
  
  }else{
    header('HTTP/1.1 400 - the data sent in the request is not valid', true, 400);
    die();
  }

}


function get_recipe_by_id($id){

  global $da, $url_path, $method, $request_body, $request_headers;

  $recipe = $da->get_recipe_by_id($id);

  // TODO: we may want to check the Accept header in the request, and if it's set to application/xml, we might return the data as XML instead of JSON
  // But for now, we'll just return the Recipe data as JSON
  
  if($recipe){
   // header("Content-Type","application/json");
    die(json_encode($recipe));
  }else{
    header('HTTP/1.1 400 - the recipe id in the requested url is not in the database', true, 400);
    die();
  }

}


function update_recipe(){

  global $da, $url_path, $method, $request_body, $request_headers;

  if($recipe = json_decode($request_body, true)){

    $recipe = $da->update_recipe($recipe);

    if($recipe){
    // header("Content-Type","application/json");
      die(json_encode($recipe));
    }else{
      header('HTTP/1.1 500 - Unable to update recipe in the database', true, 500);
      die();
    }

  }else{
    header('HTTP/1.1 400 - Invalid recipe data in the request body', true, 400);
    die();
  }

}
?>