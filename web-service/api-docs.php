<!DOCTYPE html>
<html>
<head>
	<title>Recipe Service API Docs</title>
</head>
<body>

<h1>Official API Docs for the Recipe Web Service</h1>
  <table border="1">
    <tr>
      <th>API CALL (ROUTE)</th>
      <th>METHOD</th>
      <th>ACTION</th>
    </tr>
    <tr>
      <td>recipes/</td>
      <td>GET</td>
      <td>Get's all recipes</td>
    </tr>
    <tr>
      <td>recipes/</td>
      <td>POST</td>
      <td>
        Inserts a new recipe into the data base <br>
        Expects the Content-Type to be application/json
      </td>
    </tr>
    <tr>
      <td>recipes/?order_by=x</td>
      <td>GET</td>
      <td>
        Get's all recipes, ordered by a property of a recipe <br>
        Note: x could be 'content' or 'title'
      </td>
    </tr>
    <tr>
      <td>recipes/x</td>
      <td>GET</td>
      <td>
        Get's a recipe by it's id property <br>
        ex: <b>recipes/1</b> would fetch the recipe with an id of 1
      </td>
    </tr>
    <tr>
      <td>recipes/x</td>
      <td>PUT</td>
      <td>
        Edits the recipe with id of x <br>
        Note: then Content-Type should be application/json
      </td>
    </tr>
  </table>
  ***By default, responses will return data in JSON format <br>
  ***BUT IF THE 'Accept' request header is set to 'application/xml' THEN WE RETURN THE DATA IN XML FORMAT

</body>
</html>