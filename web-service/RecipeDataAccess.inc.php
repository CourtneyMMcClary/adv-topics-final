<?php

class RecipeDataAccess{
	
	private $link;

	/**
	* Constructor
	* @param $link 		The connection to the database
	*/
	function __construct($link){
		$this->link = $link;
	}

	
	/**
	* Get all books
	* @param $order_by 		Optional - the sort order (title or recipe_instructions)
	* @return 2d array 		Returns an array of recipes (each recipe is an assoc array)
	*/
	function get_all_recipes($order_by = null){
		
		$qStr = "SELECT	id, title, recipe_instructions, recipe_ingredients, recipe_length, is_active FROM recipes";

		if($order_by == "title" || $order_by == "recipe_instructions"){
			$qStr .= " ORDER BY " . $order_by;
		}
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		$all_recipes = array();

		while($row = mysqli_fetch_assoc($result)){

			$recipe = array();
			$recipe['id'] = htmlentities($row['id']);
			$recipe['title'] = htmlentities($row['title']);
			$recipe['recipe_instructions'] = htmlentities($row['recipe_instructions']);
			$recipe['recipe_ingredients'] = htmlentities($row['recipe_ingredients']);
			$recipe['recipe_length'] = htmlentities($row['recipe_length']);
			$recipe['is_active'] = htmlentities($row['is_active']);
			

			$all_recipes[] = $recipe;
		}

		return $all_recipes;
	}


	/**
	* Get a recipe by its id
	* @param $id 			The id of the recipe to get
	* @return array 		An assoc array that has keys for each property of the recipe
	*/
	function get_recipe_by_id($id){
		

		$qStr = "SELECT	id, title, recipe_instructions, recipe_ingredients, recipe_length, is_active  FROM recipes WHERE id = " . mysqli_real_escape_string($this->link, $id);

				
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result->num_rows == 1){
			$row = mysqli_fetch_assoc($result);

			$recipe = array();
			$recipe['id'] = htmlentities($row['id']);
			$recipe['title'] = htmlentities($row['title']);
			$recipe['recipe_instructions'] = htmlentities($row['recipe_instructions']);
			$recipe['recipe_ingredients'] = htmlentities($row['recipe_ingredients']);
			$recipe['recipe_length'] = htmlentities($row['recipe_length']);
			$recipe['is_active'] = htmlentities($row['is_active']);
			return $recipe;

		}else{
			return null;
		}
			
	}


	function insert_recipe($recipe){

		// prevent SQL injection
		$recipe['title'] = mysqli_real_escape_string($this->link, $recipe['title']);
		$recipe['recipe_instructions'] = mysqli_real_escape_string($this->link, $recipe['recipe_instructions']);
		$recipe['recipe_ingredients'] = mysqli_real_escape_string($this->link, $recipe['recipe_ingredients']);
		$recipe['recipe_length'] = mysqli_real_escape_string($this->link, $recipe['recipe_length']);
		//$recipe['is_active'] = mysqli_real_escape_string($this->link, $recipe['is_active']);
		
	
		$qStr = "INSERT INTO recipes (
					title,
					recipe_instructions,
					recipe_ingredients,
					recipe_length
					
				) VALUES (
					'{$recipe['title']}',
					'{$recipe['recipe_instructions']}',
					'{$recipe['recipe_ingredients']}',
					'{$recipe['recipe_length']}'
					
				)";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the recipe id that was assigned by the data base
			$recipe['id'] = mysqli_insert_id($this->link);
			// then return the recipe
			return $recipe;
		}else{
			$this->handle_error("unable to insert recipe");
		}

		return false;
	}

	function update_recipe($recipe){

		// prevent SQL injection
		$recipe['id'] = mysqli_real_escape_string($this->link, $recipe['id']);
		$recipe['title'] = mysqli_real_escape_string($this->link, $recipe['title']);
		$recipe['recipe_instructions'] = mysqli_real_escape_string($this->link, $recipe['recipe_instructions']);
		$recipe['recipe_ingredients'] = mysqli_real_escape_string($this->link, $recipe['recipe_ingredients']);
		$recipe['recipe_length'] = mysqli_real_escape_string($this->link, $recipe['recipe_length']);
		//$recipe['is_active'] = mysqli_real_escape_string($this->link, $recipe['is_active']);
		

		$qStr = "UPDATE recipes SET title='{$recipe['title']} ', recipe_instructions='{$recipe['recipe_instructions']}', recipe_ingredients='{$recipe['recipe_ingredients']}', 
		recipe_length='{$recipe['recipe_length']}' WHERE id = " . $recipe['id'];
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $recipe;
		}else{
			$this->handle_error("unable to update recipe");
		}

		return false;
	}
	


}